import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

	private double rPart;
	private double iPart;
	private double jPart;
	private double kPart;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
	   
	   rPart = a;
	   iPart = b;
	   jPart = c;
	   kPart = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return rPart; 
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return iPart;
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return jPart; 
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return kPart; 
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
	   String result;
	   // StringBuilder?
	   result = getRpart() + "+" + getIpart() + "i+" + getJpart() + "j+" + getKpart() + "k";
      return result; 
   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      String[] split = s.trim().split("[^-0-9.]+");
      Double[] splitDouble = new Double[split.length];
      
      if (splitDouble.length < 4) {
    	  throw new RuntimeException("Etteantud stringis " + s + " on liiga vähe arve kvaterniooni loomiseks: " 
    			  					+ splitDouble.length + ". Vajalik arv: 4");
      }
      
      for (int i = 0; i < split.length; i++){
    	  splitDouble[i] = Double.parseDouble(split[i]);
    	  
      }
      
      Quaternion result = new Quaternion(splitDouble[0], splitDouble[1], splitDouble[2], splitDouble[3]);
      
      String check = result.toString();
      
      if (!(check.equals(s))){
    	  throw new IllegalArgumentException("Etteantud string " + s + " ei kujuta kvaterniooni.");
      }
      
	   return result;
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
				
	   Quaternion result = new Quaternion(this.getRpart(), this.getIpart(), this.getJpart(), this.getKpart());
	   
	   return result;
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   
   // Final idea for solution from http://stackoverflow.com/questions/1088216/whats-wrong-with-using-to-compare-floats-in-java  
   public boolean isZero() {
	  double a = Math.abs(this.getRpart());
	  double b = Math.abs(this.getIpart());
	  double c = Math.abs(this.getJpart());
	  double d = Math.abs(this.getKpart());
	  
	  double sum = a + b + c + d;
	  
	  if (sum < 0.00000001){
		  return true;
	  } else {
		  return false;
	  }
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
	   if (!(this instanceof Quaternion)){
		   throw new RuntimeException("Antud objektiga ei saa rakendada meetodit conjugate().");
	   }
	   
	   double a = this.getRpart();
	   double b = this.getIpart() * -1;
	   double c = this.getJpart() * -1;
	   double d = this.getKpart() * -1;
	   	   
      return new Quaternion(a, b, c, d); 
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
	   if (!(this instanceof Quaternion)){
		   throw new RuntimeException("Antud objektiga ei saa rakendada meetodit opposite().");
	   }
	   
	   double a = this.getRpart() * -1;
	   double b = this.getIpart() * -1;
	   double c = this.getJpart() * -1;
	   double d = this.getKpart() * -1;
	   	   
      return new Quaternion(a, b, c, d);
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
	   double a = this.getRpart() + q.getRpart();
	   double b = this.getIpart() + q.getIpart();
	   double c = this.getJpart() + q.getJpart();
	   double d = this.getKpart() + q.getKpart();
	   
	   return new Quaternion(a, b, c, d);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
	   double a1 = this.getRpart();
	   double b1 = this.getIpart();
	   double c1 = this.getJpart();
	   double d1 = this.getKpart();
	   double a2 = q.getRpart();
	   double b2 = q.getIpart();
	   double c2 = q.getJpart();
	   double d2 = q.getKpart();
	   
	   double newA = (a1*a2) - (b1*b2) - (c1*c2) - (d1*d2);   
	   double newB = (a1*b2) + (b1*a2) + (c1*d2) - (d1*c2);
	   double newC = (a1*c2) - (b1*d2) + (c1*a2) + (d1*b2);
	   double newD = (a1*d2) + (b1*c2) - (c1*b2) + (d1*a2);	   
	   
	   return new Quaternion(newA, newB, newC, newD);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
	   double a = this.getRpart() * r;
	   double b = this.getIpart() * r;
	   double c = this.getJpart() * r;
	   double d = this.getKpart() * r;
	   
	   return new Quaternion(a, b, c, d);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
	   if (this.isZero()){
		   throw new RuntimeException("0-kvaterniooni puhul ei saa kasutada meetodit inverse().");
	   }
	   
	   double a = this.getRpart();
	   double b = this.getIpart();
	   double c = this.getJpart();
	   double d = this.getKpart();
	   
	   double newA = a / ((a*a) + (b*b) + (c*c) + (d*d));
	   double newB = (b * -1) / ((a*a) + (b*b) + (c*c) + (d*d));
	   double newC = (c * -1) / ((a*a) + (b*b) + (c*c) + (d*d));
	   double newD = (d * -1) / ((a*a) + (b*b) + (c*c) + (d*d));
	   
	   return new Quaternion(newA, newB, newC, newD);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
	  double a = this.getRpart() - q.getRpart();
	  double b = this.getIpart() - q.getIpart(); 
	  double c = this.getJpart() - q.getJpart();
	  double d = this.getKpart() - q.getKpart();
	   
      return new Quaternion(a, b, c, d);	   
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
	   if (q.isZero()){
		   throw new RuntimeException("Kvaternioon " + q.toString() + 
				   					" kasutamine divideByRight() meetodiga tähendaks nulliga jagamist.");
	   }
	   Quaternion result = this.times(q.inverse());
	   
	   return result;
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
	   if (q.isZero()){
		   throw new RuntimeException("Kvaternioon " + q.toString() + 
				   					" kasutamine divideByLeft() meetodiga tähendaks nulliga jagamist.");
	   }
	   Quaternion result = q.inverse().times(this);
	   
	   return result;
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
//      Quaternion tempQo = (Quaternion) qo;
//      double a = Math.abs(this.getRpart() - tempQo.getRpart());
//      double b = Math.abs(this.getIpart() - tempQo.getIpart());
//      double c = Math.abs(this.getJpart() - tempQo.getJpart());
//      double d = Math.abs(this.getKpart() - tempQo.getKpart());
//      
//      if ((a < 0.00000001) && (b < 0.00000001) && (c < 0.00000001) && (d < 0.00000001)){
//    	  return true;
//      } else {
//    	  return false;
//      }
	   Quaternion tempQo = (Quaternion) qo;
	   if (this.minus(tempQo).isZero()){
		   return true;
	   } else {
		   return false;
	   }
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
	   
	   Quaternion step1 = this.times(q.conjugate());
	   Quaternion step2 = q.times(this.conjugate());
	   Quaternion step3 = step1.plus(step2);
	   Quaternion step4 = step3.times(0.5);
	   
	   return step4;
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
	   int hashCode = this.toString().hashCode();
	   
	   return hashCode;
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
	   double a = this.getRpart();
	   double b = this.getIpart();
	   double c = this.getJpart();
	   double d = this.getKpart();
	   
	   double result = Math.sqrt((a*a) + (b*b) + (c*c) + (d*d));
	   
	   return result;
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
	  
//	   Quaternion test1 = new Quaternion (-2., 3., 4., 5.);
//	   System.out.println(test1);
//	   System.out.println(Quaternion.valueOf(test1.toString()));
	   
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}
// end of file
